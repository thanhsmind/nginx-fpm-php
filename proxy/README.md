## **Pull https-proxy**

`$ git clone git@git.anphabe.net:devops/https-proxy.git`

## Install https-proxy using docker-compose

`$ cd https-proxy`

`$ docker-compose up`

## Install https-proxy using docker

**Build container nginx_proxy**

`$ cd https-proxy`

`$ docker build -t anphabe/nginx-proxy .`

`$ docker run -d --name nginx_proxy -e VIRTUAL_PROTO=https -p 80:80 -p 443:443 -v /var/run/docker.sock:/tmp/docker.sock:ro anphabe/nginx-proxy`

**Build container VIRTUAL_HOST**

`$ docker run  -d --name mio -e VIRTUAL_HOST=*.anphabe.dev,*.event.dev,*.miomail.dev,*.happniessatwork.dev,*.survey.dev -v /data:/data anphabe/apache`


## **Add RootCA for browser**

**ex**: Chrome browser

Setting -> HTTPS/SSL -> Manage certificates -> choose tab Authorities -> Import rootCA.pem in directory  $PWD/https-proxy/certs -> check all of options.


